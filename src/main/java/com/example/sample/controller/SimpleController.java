package com.example.sample.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@Controller
@RequestMapping("/user")
public class SimpleController {


    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String index() {

        return "login";
    }
}
